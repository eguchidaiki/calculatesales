package jp.alhinc.eguchi.daiki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ
		BufferedReader br = null;
		BufferedReader salesbr = null;
		HashMap<String, String>map = new HashMap<String,String>();
		HashMap<String, Long>salesmap = new HashMap<String,Long>();

		//支店定義ファイルの読み込み
		try {
			File file = new File(args[0],"branch.lst");
			FileReader fr =new FileReader(file);
			br = new BufferedReader(fr);
			String line ;
			while((line = br.readLine()) !=null ) {  

				//コードと支店名の保持
				String[] code = line.split(",");
				map.put(code[0],code[1]);
				salesmap.put(code[0],0L);
				
				
				if(!(code[0].matches("[0-9]{3}"))) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				if(code.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
			}
			//支店定義ファイルが存在しない
		} catch (FileNotFoundException e) {
			System.out.println("支店定義ファイルが存在しません");
			return;

			// 支店定義ファイルのフォーマット不正		
		} catch (IndexOutOfBoundsException e) {
			System.out.println("支店定義ファイルのフォーマットが不正です");
			return;
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
			return;
		}finally{
			if(br!= null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
				}
			}
		}

		//拡張子がrcdかつファイル名が数字8桁の検索
		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File file, String str) {   
				if(str.matches("[0-9]{8}.rcd")) {            
					return true; 
				}else {
					return false;
				}  
			}
		};

		//売上ファイルの読み込み、支店コード売上額の抽出
		File[] files =new File(args[0]).listFiles(filter);
		//連番エラー処理 
        
		String error = files[0].getName(); 
		String[] errorfile = error.split("[\\.]");
		long change = Long.parseLong(errorfile[0]); 

		String errorlast = files[files.length-1].getName(); 
		String[] errorfilelast = errorlast.split("[\\.]");
		long changelast = Long.parseLong(errorfilelast[0]);

		if (files.length != changelast-change+1) {
			System.out.println("売上ファイル名が連番になっていません");
			return;
		}
		for(int i=0; i<files.length; i++) {               
			int lineCount =0;
			String overerror = files[i].getName();
			ArrayList<String> saleslist = new ArrayList<>();   
			try {
				FileReader salesfr = new FileReader(files[i]);
				salesbr = new BufferedReader(salesfr);
				String salesline ;
				while((salesline = salesbr.readLine()) != null) {
					saleslist.add(salesline);
					lineCount++;
				}
				
				//ファイルの行数が3行を超えた場合				          
				if(lineCount>=3) {
					System.out.println(overerror+"のフォーマットが不正です");
					return;
				}

				//支店に該当が無かった場合	 
				if(!(map.containsKey(saleslist.get(0)))) {
					System.out.println(overerror+"の支店コードが不正です");
					return;
				}     
				long get1 = Long.parseLong(saleslist.get(1));                   
				salesmap.put(saleslist.get(0),get1+salesmap.get(saleslist.get(0)));

				//10桁を超えた場合のエラー処理
				int total = String.valueOf(get1+salesmap.get(saleslist.get(0))).length();
				if(total>10) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}    

			}catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました。");
			}finally{
				if(salesbr!= null) {
					try {
						salesbr.close();
					}catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
					}
				}
			}

		}




		//集計結果出力        
		try {
			File sumfile = new File(args[0],"branch.out");
			FileWriter fw = new FileWriter(sumfile);
			BufferedWriter bw = new BufferedWriter(fw);

			for (Map.Entry<String, Long> entry :salesmap.entrySet()){                                    
				bw.write(entry.getKey()+ "," + map.get(entry.getKey()) + ","+ entry.getValue());
				bw.newLine();
			}
			bw.close();
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
		}

	}
}


